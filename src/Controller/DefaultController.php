<?php

namespace App\Controller;

use App\Entity\Demo;
use App\Form\DemoFormType;
use App\Manager\DemoManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/maleteo", name="homepage")
     */
    public function index(Request $request, EntityManagerInterface $em, DemoManager $demoManager)
    {
        $formDemo = $this->createForm(DemoFormType::class);
        $formDemo->handleRequest($request);

        if($formDemo->isSubmitted()) {
            $demo = $formDemo->getData();
            //dd($demo);
            $em->persist($demo);
            $em->flush();
            $this->addFlash('success', 'La solicitud de demo se ha registrado correctamente');

            return $this->redirectToRoute('lista-solicitudes');
        }

        return $this->render(
            'default/index.html.twig',
            [
                'formDemo' => $formDemo->createView()
            ]
        );
    }

    /**
     * @Route("/demo-insert")
     */
    /*public function demoInsert(EntityManagerInterface $em)
    {
        $demo = new Demo();
        $demo->setName('Carlos');
        $demo->setEmail('a@gmail.com');
        $demo->setCity('Madrid');

        $em->persist($demo);
        $em->flush();

        return new Response('Demo Guardada');
    }*/

    /**
     * @Route("/maleteo/solicitudes", name="lista-solicitudes")
     */
    public function listSolicitudes(EntityManagerInterface $em)
    {
        $solicitudes = $em->getRepository(Demo::class)->findAll();

        return $this->render(
            'default/list.html.twig',
            [
                'solicitudes' => $solicitudes
            ]
        );

    }
}
