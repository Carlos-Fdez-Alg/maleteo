<?php


namespace App\Form;


use App\Entity\Demo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class DemoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'attr' => [
                'class' => 'the-form__input',
                'placeholder' => 'Carlos Vela',
                'id' => 'name'
            ]
        ]);
        $builder->add('email', EmailType::class, [
            'attr' => [
                'class' => 'the-form__input',
                'placeholder' => 'carlosV@gmail.com',
                'id' => 'email'
            ]
        ]);
        $builder->add('city', ChoiceType::class, [
            'choices' => [
                'Madrid' => 'Madrid',
                'Barcelona' => 'Barcelona',
                'Valencia' => 'Valencia',
                'Sevilla' => 'Sevilla',
                'Bilbao' => 'Bilbao',
                'Zaragoza' => 'Zaragoza',
                'Donosti' => 'Donosti',
            ],
            'placeholder' => 'Elige ciudad',
            'attr' => [
                'id' => 'city',
                'class' => 'the-form__input'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Demo::class
            ]
        );
    }
}