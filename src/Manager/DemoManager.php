<?php


namespace App\Manager;


use App\Entity\Demo;

class DemoManager
{
    private $formularioDataPath;

    public function __construct($formularioDataPath)
    {
        $this->formularioDataPath = $formularioDataPath;
    }

    public function guardarDemo(Demo $demo)
    {
        $arrayData = [];

        if (file_exists($this->formularioDataPath)) {
            $rawData = file_get_contents($this->formularioDataPath);
            $arrayData = json_decode($rawData, true);
        }


        $arrayData[] = [
            'id' => $demo->getId(),
            'titulo' => $demo->getTitulo(),
            'sinopsis' => $demo->getSinopsis(),
            'urlImagen' => $demo->getUrlImagen()
        ];

        file_put_contents($this->peliculasDataPath, json_encode($arrayData));
    }

    public function modificarPelicula(Pelicula $pelicula)
    {
        //modificarla
    }

    public function getPeliculas(): array
    {
        $peliculas = [];

        if (!file_exists($this->peliculasDataPath)) {
            return [];
        }

        $contents = file_get_contents($this->peliculasDataPath);

        $data = json_decode($contents, true);

        foreach ($data as $peliculaData) {
            $peliculas[] = new Pelicula($peliculaData['id'],$peliculaData['titulo'],$peliculaData['sinopsis'],$peliculaData['urlImagen']);
        }

        return $peliculas;
    }
}